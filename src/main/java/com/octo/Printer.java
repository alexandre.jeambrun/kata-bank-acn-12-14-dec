package com.octo;

public interface Printer {
    void print(String message);
}
