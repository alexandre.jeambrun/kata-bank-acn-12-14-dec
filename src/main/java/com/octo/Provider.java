package com.octo;

import java.time.LocalDate;

public interface Provider {
    LocalDate getDate();
}
