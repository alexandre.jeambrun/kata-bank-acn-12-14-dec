package com.octo;

public class DepotException extends Exception {

	private final static String MESSAGE = "Le dépot ne peut pas être supérieur à 5000";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DepotException() {
		super(MESSAGE);
	}
}
