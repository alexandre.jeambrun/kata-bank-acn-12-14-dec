package com.octo;

import java.util.ArrayList;
import java.util.List;

public class Compte {

	private static final double DEPOT_MAX = 5000;
    private final Provider provider;
    private double solde;
    private final ArrayList<Operation> operations = new ArrayList<>();
	private String iban;
	private Transfereur transferer;

    public Compte(double solde, Provider provider, Transfereur transferer) {
        this.solde = solde;
        this.provider = provider;
        this.transferer = transferer;
    }
    
    public Compte(double solde, Provider provider) {
        this.solde = solde;
        this.provider = provider;
    }

    public Compte(Provider provider) {
        this.provider = provider;
        this.solde = 0;
    }

    public double getSolde() {
        return solde;
    }

	public void depotArgent(double depot) throws DepotException {
		if( isInferieurOuEgalDepotMax( depot ) ) {
			solde += depot;
            ajouterDepot(depot);
		} else {
			throw new DepotException();
		}
	}

    private void ajouterDepot(double depot) {
         operations.add(new Depot(provider.getDate(), depot));
    }

    public void retraitArgent(double retrait) throws RetraitException {
        if (isSoldeSuffisant(retrait)) {
            solde -= retrait;
            ajouterRetrait(retrait);
        } else {
            throw new RetraitException();
        }
    }

    private void ajouterRetrait(double retrait) {
        operations.add(new Retrait(provider.getDate(), retrait));
    }

    private boolean isSoldeSuffisant(double retrait) {
        return retrait <= solde;
    }
    
    private boolean isInferieurOuEgalDepotMax( double depot ) {
    	return depot <= DEPOT_MAX;
    }

    public List<Operation> voirHistorique() {

        return operations;
    }

	public void transfert(String ibanTo, int montant)  {
		transferer.transfere( iban, ibanTo, montant );
		solde -= montant;
	}
}
