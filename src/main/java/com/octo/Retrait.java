package com.octo;

import java.time.LocalDate;

public class Retrait extends Operation {

	public Retrait( LocalDate date, double montant ) {
		super(date, "retrait", montant);
	}
	
}
