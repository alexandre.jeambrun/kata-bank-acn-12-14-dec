package com.octo;

public class RetraitException extends Exception{

    private final static String MESSAGE = "Le retrait ne peut pas être supérieur au solde";
    public RetraitException() {
        super(MESSAGE);
    }
}
