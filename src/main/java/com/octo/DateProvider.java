package com.octo;

import java.time.LocalDate;

public class DateProvider implements Provider {
     @Override
     public LocalDate getDate() {
        return LocalDate.now();
    }
}
