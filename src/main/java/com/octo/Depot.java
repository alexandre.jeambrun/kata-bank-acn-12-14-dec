package com.octo;

import java.time.LocalDate;

public class Depot extends Operation {

	public Depot( LocalDate date, double montant ) {
		super(date, "dépot", montant);
	}
	
}
