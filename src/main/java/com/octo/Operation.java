package com.octo;

import java.time.LocalDate;
import java.util.Objects;

public class Operation {

	private LocalDate date;
	private String typeOperation;
	private double montant;
	

	public Operation(LocalDate date, String typeOperation, double montant) {
		super();
		this.date = date;
		this.typeOperation = typeOperation;
		this.montant = montant;
	}


	@Override
	public int hashCode() {
		return Objects.hash(date, montant, typeOperation);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Operation))
			return false;
		Operation other = (Operation) obj;
		return Objects.equals(date, other.date)
				&& Double.doubleToLongBits(montant) == Double.doubleToLongBits(other.montant)
				&& Objects.equals(typeOperation, other.typeOperation);
	}


	public Operation(){
	
	}



}
