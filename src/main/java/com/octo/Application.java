package com.octo;

import java.util.Scanner;

public class Application {

    Printer printer;
    private Listener listener;

    public Application(Printer printer, Listener listener) {
        this.printer = printer;
        this.listener = listener;
    }

    public void loop() throws DepotException {
        printer.print("Bonjour client que veux-tu faire :");
        final var compte = new Compte(new DateProvider());

        String saisie = listener.getSaisie();

        if(("solde").equals(saisie)){
             printer.print("Le solde est " + compte.getSolde());
        }
        if ("depot".equals(saisie)) {
            printer.print("Quel montant souhaitez-vous d�poser ?");
            String montant = listener.getSaisie();
            compte.depotArgent(Double.parseDouble(montant));
            printer.print("Le solde est " + compte.getSolde());

        }


    }

    private String getSaisie() {
        Scanner scanner = new Scanner(System.in);
        String saisie = scanner.nextLine();
        return saisie;
    }

    private void print(String message) {
        System.out.println(message);
    }

    private void printPourLesTests(String msg) {
        String message = msg;
    }


    public static void main(String[] args) throws DepotException {
        new Application(new ConsolePrinter(), new ConsoleListener()).loop();
    }
}

class ConsolePrinter implements Printer {
    @Override
    public void print(String message) {
        System.out.println(message);
    }

}

class ConsoleListener implements Listener {
    @Override
    public String getSaisie() {
        Scanner scanner = new Scanner(System.in);
        String saisie = scanner.nextLine();
        return saisie;
    }
}
