package com.octo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CompteTest {

    private Compte compte;

    @BeforeEach
    void init(){

        compte = new Compte(new DateProviderForTest());
    }

    @Test
    public void voir_solde_du_compte_a_zero() {

        //Given
        double resultat;

        //When
        resultat = compte.getSolde();

        // Then
        assertEquals(0, resultat);

    }

    @Test
    public void voir_solde_du_compte_a_1000() {

        //Given
        double resultat;
        Compte compte = new Compte(1000, new DateProvider());

        //When
        resultat = compte.getSolde();

        // Then
        assertEquals(1000, resultat);
    }
    
    @Test
    public void solde_du_compte_doit_etre_a_100_apres_depot_de_100() throws DepotException {
    	
    	//Given
        double resultat;

        //When
        compte.depotArgent(100);
    	resultat = compte.getSolde();
    	
    	//Then    	
		assertEquals(100,resultat);
    }

    @Test
    public void voir_solde_du_compte_apres_retrait_200() throws DepotException, RetraitException {

        //Given
        double resultat;

        //When
        compte.depotArgent(300);
        compte.retraitArgent(200);
        resultat = compte.getSolde();

        //Then
        assertEquals(100,resultat);
    }

    @Test
    public void empecher_retrait_si_solde_insuffisant() throws DepotException {

        //Given
        compte.depotArgent(300);
        //When - Then
        assertThrows(RetraitException.class, () -> compte.retraitArgent(301));
    }
    
    @Test
    public void empecher_depot_de_plus_de_5000() {
    	
    	//Given
        //When
    	
    	//Then
		assertThrows(DepotException.class, () -> compte.depotArgent(5001));
    }

    @Test
    public void afficher_l_historique_sans_operation(){
        //Given

        //When
        List<Operation> resultat = compte.voirHistorique();

        //Then
        assertEquals(new ArrayList<>(), resultat);
    }

    @Test
    public void afficher_l_historique_avec_une_operation_de_depot() throws DepotException {
        //Given

        compte.depotArgent(4000);
        //When
        List<Operation> resultat = compte.voirHistorique();

        //Then
        assertEquals(1, resultat.size());
    }

    @Test
    public void afficher_historique_avec_contenu_d_un_depot() throws DepotException {
        
    	//Given

        compte.depotArgent(4000);
		Operation operation = new Depot(LocalDate.of(2022,12,15), 4000 );
        
        //When        
        
        List<Operation> resultat = compte.voirHistorique();


		//Then
        assertEquals(operation, resultat.get(0));
    	
    }

    @Test
    public void afficher_historique_avec_contenu_d_un_retrait() throws RetraitException, DepotException {

        //Given

        compte.depotArgent(4000);
        compte.retraitArgent(3000);
        Operation operation = new Retrait(LocalDate.of(2022,12,15), 3000 );

        //When

        List<Operation> resultat = compte.voirHistorique();


        //Then
        assertEquals(operation, resultat.get(1));

    }

    @Test
    public void veriifer_que_transfereur_appele_lors_dun_transfert() throws DepotException {
    	// Given
    	Transfereur transfereur = new Transfereur();
    	compte = new Compte( 4000, new DateProvider( ), transfereur );
    	
    	// When
    	compte.transfert( "ibanTo", 100 );
    	
    	// Then
    	assertEquals( 100, transfereur.getMontant() );
    }
    
}

class DateProviderForTest implements Provider {
    @Override
    public LocalDate getDate() {
        return LocalDate.of(2022, 12, 15);
    }
}
