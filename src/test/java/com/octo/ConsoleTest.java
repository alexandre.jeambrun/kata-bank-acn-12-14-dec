package com.octo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ConsoleTest {

    @Test
    void affiche_message_d_accueil() throws DepotException {
        //Arrange
        final var printer = new PrinterForTest();
        Listener listener = new ListenerForTest();

        new Application(printer, listener).loop();
        //Act
        String resultat = printer.messages.get(0);

        //Assert
        Assertions.assertEquals("Bonjour client que veux-tu faire :", resultat);
    }


    @Test
    void affiche_le_solde_si_l_utilisateur_ecrit_solde() throws DepotException {
        //Arrange

        final var printer = new PrinterForTest();
        ListenerForTest listener = new ListenerForTest();
        listener.setSaisie("solde");
        new Application(printer, listener).loop();
        //Act


        String resultat = printer.messages.get(1);

        //Assert
        Assertions.assertEquals("Le solde est 0.0", resultat);
    }


    @Test
    void affiche_le_solde_augmente_de_100_apres_le_depot_de_100() throws DepotException {
        //Arrange
        final var printer = new PrinterForTest();
        ListenerForTest listener = new ListenerForTest();
        listener.setSaisie("depot");
        listener.setSaisie("100");
        new Application(printer, listener).loop();
        //Act
        String resultat = printer.messages.get(2);

        //Assert
        Assertions.assertEquals("Le solde est 100.0", resultat);
    }

    private class ListenerForTest implements Listener {


    	private List<String> saisies = new ArrayList<>();
    	private int compteur;
        @Override
        public String getSaisie() {
            if (!this.saisies.isEmpty()) {
                return this.saisies.get(compteur++);

            } else {
                return "";
            }
        }

        public void setSaisie(String saisie) {

			this.saisies.add(saisie);

        }
    }
}

class PrinterForTest implements Printer {

    List<String> messages = new ArrayList<>();
    @Override
    public void print(String message) {
        messages.add(message);
    }
}
